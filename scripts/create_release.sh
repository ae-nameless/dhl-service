set -x

DHL_VERSION=$1

curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
     --data "{ \"name\": \"Release v${DHL_VERSION}\", \"tag_name\": \"v${DHL_VERSION}\", \"description\": \"Release v${DHL_VERSION}\", \"ref\": \"master\", \"assets\": { \"links\": [{ \"name\": \"JAR File\", \"url\": \"https://gitlab.com/ae-nameless/dhl-service/-/jobs/${CI_JOB_ID}/artifacts/file/target/dhl-service-${DHL_VERSION}.jar\" }] } }" \
     --request POST https://gitlab.com/api/v4/projects/14218198/releases
