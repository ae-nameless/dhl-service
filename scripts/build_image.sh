DHL_VERSION=$1

docker image build -t registry.gitlab.com/ae-nameless/dhl-service:$DHL_VERSION .

docker push registry.gitlab.com/ae-nameless/dhl-service