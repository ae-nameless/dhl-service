# DHL Service

Documentación oficial del servicio de DHL.

# Contenido

1. [Uso](#uso)  
    1.1. [Java](#java)  
    1.2. [Docker](#docker)  
    1.3. [Valores configurables](#valores-configurables)  
    1.4. [Base de datos](#base-de-datos)  

2. [Operaciones](#operaciones)  
    2.1. [fullfillShipment](#fullfillShipment)  
    2.2. [checkShipmentStatus](#checkShipmentStatus)  
    2.3. [cancelShipment](#cancelShipment)  

3. [Releases](#releases)

# Uso

Hay dos formas de poder usar el servicio de DHL, ejecutando directamente el jar o por medio de un contenedor Docker. A continuación se explican ambas formas.

## Java

### Requisitos

- Java 8

### Ejecución

Para la ejecución del jar, este se puede descargar de la página de releases (https://gitlab.com/ae-nameless/dhl-service/-/releases) del servicio. Se recomienda descargar la última versión (usando el sistema Semantic Versioning).

Para su ejecución con todos los valores por defecto:

`java -jar dhl-service.jar`

Este comando levantará un servidor tomcat el cual sirve un wsdl en: `http://localhost:8080/ws/dhl.wsdl`

## Docker

### Requisitos

- Docker

### Ejecución

Para la creación de un contenedor docker se pueden ver la lista de tags disponibles en: https://gitlab.com/ae-nameless/dhl-service/container_registry, para ello al igual que Java se recomienda usar la última versión.

Para su ejecución con todos los valores por defecto:

`docker run -d -p 8080:8080 --name dhl-service registry.gitlab.com/ae-nameless/dhl-service:1.0.4`

De esta forma, la aplicación se levantará en el puerto 8080 por defecto. Para probar ir a `http://localhost:8080/ws/dhl.wsdl`

Adicionalmente, para cambiar los valores por defecto, hay ciertas variables de entorno que permiten esto (para más detalle ver la sección **Valores configurables**). Un ejemplo cambiando las credenciales de la base de datos h2:

`docker run -d -p 8080:8080 --name dhl-service -e DHL_DB_USERNAME=dhl-user -e DHL_DB_PASSWORD=my-secret registry.gitlab.com/ae-nameless/dhl-service:1.0.4`

## Valores configurables

La aplicación permite configurar ciertas propiedades para mayor flexibilidad. La columna propiedad es útil para la ejecución usando **Java**, y las variables de entorno para la ejecución usando **Docker**.

| Propiedad | Descripción | Variable de entorno | Valor por defecto |
|---|---|---|---|
| spring.datasource.username | Si es la primera vez que se inicia la aplicación, esta propiedad permite definir el nombre de usuario para la conexión a la base de datos H2. Si no es la primera vez, debe ser el usuario que ya esté configurado para establecer la conexión a la DB. | DHL_DB_USERNAME | sa |
| spring.datasource.password | Contraseña para la conexión a la DB, si es la primera vez permite definirla, si no, es la contraseña definida anteriorment. | DHL_DB_PASSWORD | password |
| spring.h2.console.enabled | La aplicación permite habilitar o deshabilitar una consola web para consultar la base de datos de H2. Valores posibles: `[true, false]`. Si se habilita, la consola es desplegada en `http://localhost:8080/h2-console` | DHL_DB_CONSOLE | false |
| server.port | Puerto donde la aplicación estará corriendo. | DHL_SERVER_PORT | 8080 |
| dhl.scheduled.task.fixedRate | Hay una tarea programada que se encarga de actualizar los estados de los envíos, para ello, esta propiedad permite configurar el tiempo de cada cuanto se ejecutará, este tiempo debe ser configurado en milisegundos. | DHL_TASK_RATE | 360000 |

### Ejemplos

- java: `java -jar dhl-service.jar --spring.datasource.username=dhl-user --server.port=7777`
- docker: `docker run -d -p 8080:1234 --name dhl -e DHL_SERVER_PORT=1234 registry.gitlab.com/ae-nameless/dhl-service:1.0.3`

## Base de datos

La aplicación usa una base de datos H2. H2 para persistir la información crea un archivo en el mismo directorio donde la aplicación se está ejecutando, el archivo es **dhl_h2.mv.db**.

Para conectarse a la DB por medio de la consola que expone la aplicación (`http://localhost:8080/h2-console`), se deben tener los siguientes datos:

- **Driver Class:** org.h2.Driver
- **JDBC URL:** jdbc:h2:./dhl_h2;DB_CLOSE_DELAY=-1
- **User Name:** sa (o el valor configurado)
- **Password:** password (o el valor configurado)

Hay que mencionar que el JDBC URL es la ruta de la máquina donde está el archivo **dhl_h2.mv.db**. Por defecto, funciona con el valor del ejemplo de arriba.

# Operaciones

En esta sección se documentará las tres operaciones que se proveen.

## fullfillShipment

Esta operación tiene como objetivo crear la guía de envío (shipment) en DHL.

### input

Se recibe un shipment que contiene los siguientes atributos.

| Propiedad | Descripción | Tipo de valor |
|---|---|---|
| partner | Nombre de la empresa titular del B2C, ej: Nameless | String |
| supplier | Nombre del proveedor de los productos, ej: Sony | String |
| orderId | Número de la orden la cual está atada al envío. Este valor debe ser único. Este valor es usado para consultar el envío y cancelarlo. | String |
| addresseeName | Primer nombre de la persona a la que se le hace el envío (destinatario) | String |
| addresseeLastName | Primer apellido de la persona a la que se le hace el envío (destinatario) | String |
| country | País destino del envío | String |
| state | Estado o departamento destino del envío | String |
| city | Ciudad destino del envío | String |
| street | Dirección física de destino del envío | String |
| zipcode | Código postal de destino del envío | String |
| items | Lista de productos que serán enviados al destinatario | List de item |

Las propiedades de un item son:

| Propiedad | Descripción | Tipo de valor |
|---|---|---|
| itemId | Identificador único usado internamente por DHL | Integer |
| prodId | Identificador del producto para el sistema externo | String |
| productName | Nombre del producto | String |
| partNumber | Número serial del producto (si no se usa, se puede dejar vacío) | String |
| price | Valor unitario del producto | Double |
| quantity | Cantidad de referencias de este producto que irá en el envío | Integer |

### output

| Propiedad | Descripción | Tipo de valor |
|---|---|---|
| fullfillShipmentResult | Booleano indicando si la transacción de generación de guía de envío fue exitoso o no. Generalmente no es exitoso cuando se intenta crear un envío con un orderId ya existente en el sistema | Boolean |

## checkShipmentStatus

Esta operación tiene como objetivo devolver el estado actual del envío consultado.

### input

| Propiedad | Descripción | Tipo de valor |
|---|---|---|
| orderId | Número de la orden con el cual la guía de envío se generó. | String |

### output

| Propiedad | Descripción | Tipo de valor |
|---|---|---|
| checkShipmentStatusResult | Estado actual de la orden, en caso que el orderId no esté en el sistema de DHL, la respuesta es vacía. | ShipmentStatus |

Los valores de la enumeración ShipmentStatus son:

| Propiedad | Descripción |
|---|---|
| Pending | Estado inicial cuando una guía de envío recién se crea. |
| Collected | La orden ya se encuentra en la bodega de DHL del país origen. |
| In Transit | La orden está viajando hacia el país destino. |
| Arrived Hub | La orden llegó a la bodega de DHL del país destino. |
| Out For Delivery | La orden está siendo transportada a punto de ser entregada. |
| Delivered | La orden ha sido entragada al destinatario. |
| Cancelled | El envío fue cancelado. |

## cancelShipment

Esta operación tiene como objetivo poder cancelar un envío. La condición para poder cancelar un envío es que el estado actual no debe ser Delivered o Cancelled.

### input

| Propiedad | Descripción | Tipo de valor |
|---|---|---|
| orderId | Número de la orden con el cual la guía de envío se generó. | String |

### output

| Propiedad | Descripción | Tipo de valor |
|---|---|---|
| cancelShipmentResult | booleano indicando si la operación pudo ser llevada exitosamente o no. Generalmente es no exitoso si la condición mencionada arriba no se cumple | Boolean |

# Releases

| Versión | Cambios | Fecha |
|---|---|---|
| 1.0.7 | <ul><li>Se agrega un nuevo estado de envío el cual es: Cancelled.</li><li>Se agrega una nueva operación (cancelShipment) para poder cancelar un envío.</li><li>Se actualiza la documentación con más detalle sobre las operaciones que expone el servicio.</li></ul> | 01 Nov 2019 |
| 1.0.6 | Primera versión oficial | 17 Sept 2019 |