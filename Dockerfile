FROM openjdk:8
WORKDIR /dhl-service
COPY target/dhl-service*.jar dhl-service.jar
CMD ["java","-jar","dhl-service.jar"]
