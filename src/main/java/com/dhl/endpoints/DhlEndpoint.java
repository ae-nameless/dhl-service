package com.dhl.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.dhl.components.DhlFacade;
import com.dhl.schema.CancelShipmentRequest;
import com.dhl.schema.CancelShipmentResponse;
import com.dhl.schema.CheckShipmentStatusRequest;
import com.dhl.schema.CheckShipmentStatusResponse;
import com.dhl.schema.FullfillShipmentRequest;
import com.dhl.schema.FullfillShipmentResponse;

@Endpoint
public class DhlEndpoint {
	
	@Autowired
	private DhlFacade dhlService;
	
	private static final String NAMESPACE_URI = "schema.dhl.com";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "fullfillShipmentRequest")
    @ResponsePayload
    public FullfillShipmentResponse fullFillShipment(@RequestPayload FullfillShipmentRequest request) {
		Integer shipmentId = null;
		try {
			shipmentId = dhlService.registerShipment(request.getShipment());
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		FullfillShipmentResponse fulfillShipment = new FullfillShipmentResponse();
		if (shipmentId != null)
			fulfillShipment.setFullfillShipmentResult(true);
    	return fulfillShipment;
    }
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "checkShipmentStatusRequest")
    @ResponsePayload
    public CheckShipmentStatusResponse checkShipmentStatusRequest(@RequestPayload CheckShipmentStatusRequest request) {
		CheckShipmentStatusResponse  c = new CheckShipmentStatusResponse();
		c.setCheckShipmentStatusResult(dhlService.checkShipmentStatus(request.getOrderId()));
    	return c;
    }
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "cancelShipmentRequest")
    @ResponsePayload
	public CancelShipmentResponse cancelShipmentRequest(@RequestPayload CancelShipmentRequest request) {
		final CancelShipmentResponse response = new CancelShipmentResponse();
		response.setCancelShipmentResult(dhlService.cancelShipment(request.getOrderId()));
		return response;
	}
}
