package com.dhl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DhlServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DhlServiceApplication.class, args);
	}

}
