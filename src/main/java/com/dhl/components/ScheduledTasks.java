package com.dhl.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
	
	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	
	@Autowired
	private DhlFacade dhlService;

	@Scheduled(initialDelay = 60000, fixedRateString = "${dhl.scheduled.task.fixedRate}")
	public void updateShipmentStatus() {
		logger.info("Starting update of shipment's status");
		try {
			dhlService.updateShipmentStatus(logger);			
			logger.info("Scheduled task was processed successfully");
		} catch(Exception ex) {
			logger.info("Scheduled task can not be completed");
			ex.printStackTrace();
		}
	}
}
