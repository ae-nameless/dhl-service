package com.dhl.components;

import org.slf4j.Logger;

import com.dhl.schema.Shipment;

public interface DhlFacade {

	Integer registerShipment(Shipment shipment);
	
	String checkShipmentStatus(String orderId);
	
	void updateShipmentStatus(Logger logger);
	
	boolean cancelShipment(String orderId);
}
