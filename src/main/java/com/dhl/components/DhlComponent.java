package com.dhl.components;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.data.Item;
import com.dhl.data.Shipment;
import com.dhl.data.ShipmentStatus;
import com.dhl.repositories.ItemRepository;
import com.dhl.repositories.ShipmentRepository;

@Component
public class DhlComponent implements DhlFacade {

	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private ShipmentRepository shipmentRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Integer registerShipment(com.dhl.schema.Shipment schemaShipment) {
		Shipment dataShipment = convertShipmentToData(schemaShipment);
		dataShipment = shipmentRepository.save(dataShipment);			
		for(com.dhl.schema.Item schemaItem : schemaShipment.getItems().getItem()) {
			Item dataItem = convertItemToData(schemaItem);
			dataItem.setShipment(dataShipment);
			itemRepository.save(dataItem);
		}
		return dataShipment.getId();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void updateShipmentStatus(Logger logger) {
		final List<Shipment> shipments = shipmentRepository.findByStatusNotIn(Arrays.asList(ShipmentStatus.DELIVERED, ShipmentStatus.CANCELLED));
		if (shipments.isEmpty()) {
			logger.info("There are no items to update");
			return;
		}
		logger.info("Task is going to update {} shipments", shipments.size());
		for(Shipment shipment : shipments) {
			final ShipmentStatus currentStatus = shipment.getStatus();
			final ShipmentStatus nextStatus = ShipmentStatus.values()[currentStatus.ordinal() + 1];
			shipment.setStatus(nextStatus);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	public String checkShipmentStatus(final String orderId) {
		Shipment shipment = shipmentRepository.findShipmentByOrderId(orderId);
		return shipment!=null?shipment.getStatus().getCodigo():"";
	}
	
	@Transactional(rollbackFor = Exception.class)
	public boolean cancelShipment(String orderId) {
		Shipment shipment = shipmentRepository.findShipmentByOrderId(orderId);
		if (null != shipment && !shipment.getStatus().equals(ShipmentStatus.DELIVERED) && 
				!shipment.getStatus().equals(ShipmentStatus.CANCELLED)) {
			shipment.setStatus(ShipmentStatus.CANCELLED);
			shipmentRepository.save(shipment);
			return true;
		}
		return false;
	}
	
	private Shipment convertShipmentToData(com.dhl.schema.Shipment schemaShipment) {
		Shipment dataShipment = new Shipment();
		dataShipment.setAddresseeLastName(schemaShipment.getAddresseeLastName());
		dataShipment.setAddresseeName(schemaShipment.getAddresseeName());
		dataShipment.setCity(schemaShipment.getCity());
		dataShipment.setCountry(schemaShipment.getCountry());
		dataShipment.setOrderId(schemaShipment.getOrderId());
		dataShipment.setPartner(schemaShipment.getPartner());
		dataShipment.setState(schemaShipment.getState());
		dataShipment.setStreet(schemaShipment.getStreet());
		dataShipment.setSupplier(schemaShipment.getSupplier());
		dataShipment.setZipcode(schemaShipment.getZipcode());
		return dataShipment;
	}
	
	private Item convertItemToData(com.dhl.schema.Item schemaItem) {
		Item dataItem = new Item();
		dataItem.setPartNumber(schemaItem.getPartNumber());
		dataItem.setPrice(schemaItem.getPrice());
		dataItem.setProdId(schemaItem.getProdId());
		dataItem.setProductName(schemaItem.getProductName());
		dataItem.setQuantity(schemaItem.getQuantity());
		return dataItem;
	}
}
