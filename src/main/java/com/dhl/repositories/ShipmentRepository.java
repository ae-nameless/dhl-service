package com.dhl.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dhl.data.Shipment;
import com.dhl.data.ShipmentStatus;

@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Integer>{
	
	Shipment findShipmentByOrderId(String orderId);
	List<Shipment> findByStatusNotIn(List<ShipmentStatus> status);
}
