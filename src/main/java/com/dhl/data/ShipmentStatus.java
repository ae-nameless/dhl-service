package com.dhl.data;

public enum ShipmentStatus {

	PENDING("Pending"),
	COLLECTED("Collected"),
	IN_TRANSIT("In Transit"),
	ARRIVED_HUB("Arrived Hub"),
	OUT_FOR_DELIVERY("Out For Delivery"),
	DELIVERED("Delivered"),
	
	CANCELLED("Cancelled");
	
	private ShipmentStatus(String codigo) {
		this.codigo = codigo;
	}
	
	private String codigo;
	
	public String getCodigo() {
		return codigo;
	}
}
